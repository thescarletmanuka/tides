# Tidal Depth Window Calculator

Calculates times when tide is within a depth range,
given a file of high and low tides and heights.

This is currently just a personal project for deciding when to go swim lengths at a local beach.

## Sources

- Uses the formula from a PDF provided by the LINZ, labelled as from the NZ Nautical Almanic,
  which in turn has this attribution:
  "Copyright Commonwealth of Australia 2007 – Used with the permission of the Australian Hydrographic Service"

  https://www.linz.govt.nz/sites/default/files/media/doc/hydro_201920-almanac_method-to-find-times-or-heights-between-high-and-low-waters_pdf.pdf

- Takes a csv file from LINZ.
  Download the desired year from https://www.linz.govt.nz/sea/tides/tide-predictions
