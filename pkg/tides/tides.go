package tides

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"
)

// TidePivot holds the time and height of a change between tides
type TidePivot struct {
	time   time.Time
	height float32
}

// Tide holds the start and end details of single tidal phase
type Tide struct {
	start TidePivot
	end   TidePivot
}

// SwimWindow is a simple time interval
type SwimWindow struct {
	start time.Time
	end   time.Time
}

// Parse converts a standard csv file from LINZ into an array of high or low tides
// - ignores 3 descriptive lines at the top of the file
// - allows for CRLF line ends
// - handles a final LF
func Parse(bytes []byte) []TidePivot {
	lines := strings.Split(string(bytes), "\n")[3:]
	tides := []TidePivot{}
	for _, l := range lines {
		l = strings.TrimSpace(l)
		if l == "" {
			continue // probably the final line
		}
		// fmt.Println(l)
		fields := strings.Split(l, ",")
		day, err := strconv.Atoi(fields[0])
		if err != nil {
			panic(fmt.Sprintf("Invalid day: [%s]", fields[0]))
		}
		month, err := strconv.Atoi(fields[2])
		if err != nil {
			panic(fmt.Sprintf("Invalid month: %s", fields[0]))
		}
		year, err := strconv.Atoi(fields[3])
		if err != nil {
			panic(fmt.Sprintf("Invalid year: %s", fields[0]))
		}
		for n := 4; n < len(fields); n = n + 2 {
			if fields[n] == "" {
				continue // special case - when only 3 tides in a day
			}
			when := strings.Split(fields[n], ":")
			hour, err := strconv.Atoi(when[0])
			if err != nil {
				panic(fmt.Sprintf("Invalid hour: [%s]", when[0]))
			}
			minute, err := strconv.Atoi(when[1])
			if err != nil {
				panic(fmt.Sprintf("Invalid minute: %s", when[1]))
			}
			h, err := strconv.ParseFloat(fields[n+1], 32)
			if err != nil {
				panic(fmt.Sprintf("Invalid height: [%s]", fields[n+2]))
			}

			tide := TidePivot{
				time:   time.Date(year, time.Month(month), day, hour, minute, 0, 0, time.UTC),
				height: float32(h),
			}
			tides = append(tides, tide)
			// fmt.Printf("%s, %f.02\n", tide.time, tide.height)
		}
	}
	return tides
}

// CollateTideIntervals turns a list of high or low tide points into a list of intervals between high/low tide
func CollateTideIntervals(tides []TidePivot) []Tide {
	intervals := make([]Tide, len(tides)-1)
	for idx := range intervals {
		intervals[idx] = Tide{start: tides[idx], end: tides[idx+1]}
	}
	return intervals
}

// PrintTideIntervals is a debug utility to dump intervals to text
func PrintTideIntervals(intervals []Tide) {
	for _, i := range intervals {
		fmt.Println(i.start.time, i.end.time)
	}
}

func calcIntersection(i Tide, height float32) *time.Time {
	if height > i.start.height && height > i.end.height {
		return nil
	}
	if height < i.start.height && height < i.end.height {
		return nil
	}
	A := 2*math.Pi - math.Acos(float64((2*(height-i.start.height)/(i.end.height-i.start.height))-1))
	d := i.end.time.Sub(i.start.time).Hours() * (A/math.Pi - 1)
	d2 := time.Duration(d * float64(time.Hour)).Round(time.Minute)
	t := i.start.time.Add(d2)
	// fmt.Println(A, d, d2, t)
	return &t
}

// CalcSwimWindows generates a list of intervals when swimming is possible
func CalcSwimWindows(tides []Tide, minDepth float32, maxDepth float32) []SwimWindow {
	swims := []SwimWindow{}
	if len(tides) == 0 {
		return swims
	}

	// current swim interval - create new with every start, append to list on exit
	var current SwimWindow

	if tides[0].start.height < maxDepth && tides[0].start.height > minDepth {
		current = SwimWindow{start: tides[0].start.time}
	}
	for _, i := range tides {
		// Possible entrance to range.  NB: ignore equalities where just touch edge of range
		if i.start.height > maxDepth && i.end.height < maxDepth {
			current = SwimWindow{start: *calcIntersection(i, maxDepth)}
		}
		if i.start.height < minDepth && i.end.height > minDepth {
			current = SwimWindow{start: *calcIntersection(i, minDepth)}
		}
		// possible exits (must come after any entrance, as can't reverse direction within an interval)
		if i.start.height < maxDepth && i.end.height > maxDepth {
			current.end = *calcIntersection(i, maxDepth)
			swims = append(swims, current)
		}
		if i.start.height > minDepth && i.end.height < minDepth {
			current.end = *calcIntersection(i, minDepth)
			swims = append(swims, current)
		}
	}
	return swims
}

const oneDay = 24 * time.Hour // near enough for our purposes

// PrintSwims dumps times to stdout
// NB: end can be on different date that start, but should be close so just print the day
func PrintSwims(swims []SwimWindow, addGapBetweenContiguousDays bool) {
	var nextDay time.Time
	for _, s := range swims {
		if addGapBetweenContiguousDays && !nextDay.IsZero() && nextDay.Before(s.start.Truncate(oneDay)) {
			fmt.Println()
		}
		// fmt.Printf("%s,%s,%s,%s\n", s.start.Format("Mon Jan 2"), s.start.Format("15:04"), s.end.Format("Mon"), s.end.Format("15:04"))
		fmt.Printf("%s   \t%s\t%s-%s\n", s.start.Format("Mon"), s.start.Format("Jan 02"), s.start.Format("15:04"), s.end.Format("15:04")) // tuned for Google Keep with variable font
		nextDay = s.start.Add(oneDay).Truncate(oneDay)
	}
}

// Filter trims out swims which end too early, start too late, or are too short
// NB:
// - am not checking that overlap with target period is > min duration.  Change period boundaries for same effect.
// - behaviour for overnight windows is not really tested
func Filter(swims []SwimWindow, start, end string, minDuration time.Duration) []SwimWindow {
	var filtered []SwimWindow
	for _, s := range swims {
		if s.start.Day() == s.end.Day() {
			// keep same-day window if any part falls within target period and has min duration.
			if s.end.Format("15:04") < start || s.end.Format("15:04") > end || s.end.Sub(s.start) < minDuration {
				continue
			}
		} else {
			// keep overnight window if matches on either end.  Don't bother checking for min duration extension over threshold.
			if s.start.Format("15:04") > end && s.end.Format("15:04") < start {
				continue
			}
		}
		filtered = append(filtered, s)
	}
	return filtered
}
