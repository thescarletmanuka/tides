package tides

import (
	"os"
	"testing"
	"time"
)

func TestParser(t *testing.T) {
	bytes, err := os.ReadFile("test_data/full")
	if err != nil {
		t.Fatal("Unable to read data file")
	}
	result := Parse(bytes)
	expected := []TidePivot{
		//NB - times are UTC so they truncate cleanly.
		//     time zones are not otherwise relevant
		{time.Date(2020, 1, 1, 3, 9, 0, 0, time.UTC), 3.6},
		{time.Date(2020, 1, 1, 9, 17, 0, 0, time.UTC), 1.1},
		{time.Date(2020, 1, 1, 15, 27, 0, 0, time.UTC), 3.7},
		{time.Date(2020, 1, 1, 21, 48, 0, 0, time.UTC), 1.2},
		{time.Date(2020, 12, 30, 5, 34, 0, 0, time.UTC), 0.8},
		{time.Date(2020, 12, 30, 11, 49, 0, 0, time.UTC), 4.0},
		{time.Date(2020, 12, 30, 17, 58, 0, 0, time.UTC), 0.9},
		{time.Date(2020, 12, 31, 0, 2, 0, 0, time.UTC), 3.9},
		{time.Date(2020, 12, 31, 6, 13, 0, 0, time.UTC), 0.7},
		{time.Date(2020, 12, 31, 12, 27, 0, 0, time.UTC), 4.0},
		{time.Date(2020, 12, 31, 18, 38, 0, 0, time.UTC), 0.8},
	}

	if len(result) != len(expected) {
		t.Fatalf("Result length is %d not %d", len(result), len(expected))
	}
	for idx := range result {
		if result[idx] != expected[idx] {
			t.Errorf("Mismatched record. %d, [%+v] != [%+v]", idx, result[idx], expected[idx])
		}
	}
}

func TestCalcIntersection(t *testing.T) {
	const h float32 = 3.8
	t1 := time.Date(2020, 12, 30, 6, 0, 0, 0, time.UTC)
	t2 := time.Date(2020, 12, 30, 7, 0, 0, 0, time.UTC)
	tMid := t1.Add(time.Duration(t2.Sub(t1) / 2))     // 1/4 rise in half the time
	tTwelth := t1.Add(time.Duration(t2.Sub(t1) / 6))  // 1/12 rise in first 6th of tide (usually 1 hr)
	tQuarter := t1.Add(time.Duration(t2.Sub(t1) / 3)) // 1/4 rise in first third (usually 2 hr)
	tReal := time.Date(2020, 12, 30, 10, 50, 0, 0, time.UTC)

	cases := []struct {
		name string
		Tide
		t *time.Time
	}{
		{
			"Never reaches height",
			Tide{
				TidePivot{t1, h - 0.2},
				TidePivot{t2, h - 0.1},
			}, nil,
		},
		{
			"Always above height",
			Tide{
				TidePivot{t1, h + 0.2},
				TidePivot{t2, h + 0.1},
			}, nil,
		},
		{
			"Height is at midpoint, incoming tide",
			Tide{
				TidePivot{t1, h - 0.2},
				TidePivot{t2, h + 0.2},
			}, &tMid,
		},
		{
			"Height is at midpoint, outgoing tide",
			Tide{
				TidePivot{t1, h + 0.2},
				TidePivot{t2, h - 0.2},
			}, &tMid,
		},
		{
			"Height at 1st hour is 1/12 total change",
			Tide{
				TidePivot{t1, h - 0.1},
				TidePivot{t2, h + 1.1},
			}, &tTwelth,
		},
		{
			"Height at 2nd hour is 1/4 total change",
			Tide{
				TidePivot{t1, h - 0.1},
				TidePivot{t2, h + 0.3},
			}, &tQuarter,
		},
		{
			"Real world example",
			Tide{
				TidePivot{time.Date(2020, 12, 30, 5, 34, 0, 0, time.UTC), 0.8},
				TidePivot{time.Date(2020, 12, 30, 11, 49, 0, 0, time.UTC), 4.0},
			}, &tReal,
		},
	}
	for _, c := range cases {
		at := calcIntersection(c.Tide, h)
		if at != nil {
			*at = (*at).Round(5 * time.Minute)
		}
		if at != c.t && (at == nil || c.t == nil || !at.Equal(*c.t)) {
			t.Errorf("%s. Unexpected tMin: %s != %s", c.name, at, c.t)
		}
	}

}
