package cli

import (
	"github.com/spf13/cobra"
)

var RootCmd = &cobra.Command{
	Use:   "tides",
	Short: "Tide performs various tide related calculations",
}
