package cli

import (
	"fmt"
	"io"
	"os"
	"time"

	"github.com/spf13/cobra"
	"puriri.nz/tides/pkg/tides"
)

var minDepth, maxDepth float32
var startTime, stopTime string
var minSwimMins int

func init() {
	RootCmd.AddCommand(calcCmd)
	calcCmd.Flags().StringVar(&startTime, "startTime", "09:00", "Earliest time you care to swim")
	calcCmd.Flags().StringVar(&stopTime, "stopTime", "18:30", "Latest time you care to swim")
	calcCmd.Flags().IntVar(&minSwimMins, "minSwimTime", 30, "Shortest window (in minutes) to report")
	calcCmd.Flags().Float32Var(&minDepth, "minDepth", 3.8, "Minimum depth worth swimming.  This likely requires an offset unless you swim right where tide heights are measured.")
	calcCmd.Flags().Float32Var(&maxDepth, "maxDepth", 4.3, "Maximum depth worth swimming.  This likely requires an offset unless you swim right where tide heights are measured.")
}

var calcCmd = &cobra.Command{
	Use:   "calc",
	Short: "Calculate swim windows",
	Long: `
Calculates intervals when a tide is within a depth range, 
given a file of tide times and high/low heights.

It ingests tide tables in csv format as provided by LINZ 
at https://www.linz.govt.nz/sea/tides/tide-predictions`,

	RunE: func(cmd *cobra.Command, args []string) error {
		bytes, err := io.ReadAll(os.Stdin)
		if err != nil {
			return fmt.Errorf("unable to read stdin: %w", err)
		}
		tidePoints := tides.Parse(bytes)
		intervals := tides.CollateTideIntervals(tidePoints)
		// tides.PrintTideIntervals(intervals)
		swims := tides.CalcSwimWindows(intervals, minDepth, maxDepth)
		swims = tides.Filter(swims, startTime, stopTime, time.Duration(minSwimMins)*time.Minute)
		tides.PrintSwims(swims, true)
		return nil
	},
}
